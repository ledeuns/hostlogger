PREFIX?=/usr/local
BINDIR=${PREFIX}/sbin
MANDIR= ${PREFIX}/man/man

PROG=	hostlogger
SRCS=	hostlogger.c
CFLAGS+= -g -Wall -I${.CURDIR} -I/usr/local/include
CFLAGS+= -Wstrict-prototypes -Wmissing-prototypes
CFLAGS+= -Wmissing-declarations
CFLAGS+= -Wshadow -Wpointer-arith -Wcast-qual
CFLAGS+= -Wsign-compare
LDADD+= -lutil
MAN=

.include <bsd.prog.mk>
