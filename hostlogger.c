     #include <sys/types.h>
     #include <sys/socket.h>
     #include <netinet/in.h>
     #include <netinet/ip.h>
     #include <netinet/ip6.h>
     #include <netinet/tcp.h>
     #include <arpa/inet.h>
     #include <stdio.h>
     #include <string.h>
     #include <err.h>
#include <errno.h>
#include <poll.h>

#define DIVERT_PORT 700
#define PFD_DIVERT_SOCKET 0
#define PFD_DIVERT6_SOCKET 1
#define POLL_MAX 2

int
main(int argc, char *argv[])
{
	struct pollfd	pfd[POLL_MAX];
             int fd, fd6, s, nfds;
             struct sockaddr_in sin;
             struct sockaddr_in6 sin6;
             socklen_t sin_len, sin6_len;
		u_int16_t extlen;

             fd = socket(AF_INET, SOCK_RAW, IPPROTO_DIVERT);
             if (fd == -1)
                     err(1, "socket");

             memset(&sin, 0, sizeof(sin));
             sin.sin_family = AF_INET;
             sin.sin_port = htons(DIVERT_PORT);

             sin_len = sizeof(struct sockaddr_in);

             s = bind(fd, (struct sockaddr *) &sin, sin_len);
             if (s == -1)
                     err(1, "bind");

             fd6 = socket(AF_INET6, SOCK_RAW, IPPROTO_DIVERT);
             if (fd6 == -1)
                     err(1, "socket");
             memset(&sin6, 0, sizeof(sin6));
             sin6.sin6_family = AF_INET6;
             sin6.sin6_port = htons(DIVERT_PORT);

             sin6_len = sizeof(struct sockaddr_in6);

             s = bind(fd6, (struct sockaddr *) &sin6, sin6_len);
             if (s == -1)
                     err(1, "bind");

	bzero(&pfd, sizeof(pfd));
	pfd[PFD_DIVERT_SOCKET].fd = fd;
	pfd[PFD_DIVERT_SOCKET].events = POLLIN;
	pfd[PFD_DIVERT6_SOCKET].fd = fd6;
	pfd[PFD_DIVERT6_SOCKET].events = POLLIN;


             for (;;) {
                     ssize_t n;
                     char packet[IP_MAXPACKET];
                     struct ip *ip;
		struct ip6_hdr *ip6_hdr;
                     struct tcphdr *th;
		     char *ptr, *p, *f;
		char host[256];
                     unsigned int hlen;
                     char src[48];

	if ((nfds = poll(pfd, POLL_MAX, -1)) == -1)
		if (errno != EINTR) {
			err(1, "poll error");
		}

	if (nfds > 0 && pfd[PFD_DIVERT_SOCKET].revents & POLLIN) {
                     memset(packet, 0, sizeof(packet));
                     n = recvfrom(fd, packet, sizeof(packet), 0, (struct sockaddr *) &sin, &sin_len);
                     if (n == -1) {
                             warn("recvfrom");
                             continue;
                     }
                     if (n < (sizeof(struct ip) + sizeof(struct tcphdr))) {
                             warnx("packet is too short");
                             continue;
                     }


                     ip = (struct ip *) packet;
                     hlen = ip->ip_hl << 2;
                     if (hlen < sizeof(struct ip) || ntohs(ip->ip_len) < hlen ||
                         n < ntohs(ip->ip_len)) {
                             warnx("invalid IPv4 packet");
                             continue;
                     }


                     th = (struct tcphdr *) (packet + hlen);
			ptr = (void*)(th) + (th->th_off<<2);
			memset(host, 0, sizeof(host));
/* HTTP packet = check Host header */
			f = strstr(ptr, "\r\n\r\n");
			if (f != NULL) {
				p = strstr(ptr, "Host: ");
				if (p != NULL) {
					f = strstr(p, "\r\n");
					if (f != NULL) {
						snprintf(host, f-p-5, "%s", p+6);
					}
				}
			} else {
/* HTTPS packet = check SNI option */
				if (ptr[0] == 0x16 && ptr[1] == 0x03 && ptr[5] == 0x01) {
					u_int8_t len8;
					u_int16_t len16;
					u_int32_t len32;
				/* SSLv3/TLS handshake & client hello.
				 * Search for Extension 0 : server_name
				 */
					p = ptr + 5; /* Start of ClientHello */
					len32 = (p[1] & 0x000000ff); len32 <<= 8;
					len32 += (p[2] & 0x000000ff); len32 <<= 8;
					len32 += (p[3] & 0x000000ff);
					if (len32 == 0) goto endv4;
					len8 = ptr[43];
					p = ptr + 43 + 1 + len8; /* SessionId */
					len16 = (p[0] & 0x00ff); len16 <<=8;
					len16 += (p[1] & 0x00ff);
					p = p + 2 + len16; /* CipherSuite */
					len8 = p[0];
					if (len8 == 0) goto endv4;
					p = p + 1 + len8; /* Compression */
					/* Extensions */
					len16 = (p[0] & 0x00ff); len16 <<=8;
					len16 += (p[1] & 0x00ff);
					extlen = len16;
					if (extlen == 0) goto endv4;
					p += 2;
					extlen -= 2;
					len16 = (p[0] & 0x00ff); len16 <<=8;
					len16 += (p[1] & 0x00ff);
					while (len16 != 0 && extlen > 0) {
						p += 2;
						len16 = (p[0] & 0x00ff); len16 <<=8;
						len16 += (p[1] & 0x00ff);
						extlen -= 2;
					}
					if (extlen == 0) goto endv4;
					else {
						p += 2;
						len16 = (p[0] & 0x00ff); len16 <<=8;
						len16 += (p[1] & 0x00ff);
						if (len16 == 0) goto endv4; // sni length
						p += 2;
						len16 = (p[0] & 0x00ff); len16 <<=8;
						len16 += (p[1] & 0x00ff);
						if (len16 == 0) goto endv4; // sni list length
						p += 4;
						len8 = p[0];
						p++;
						snprintf(host, len8+1, "%s", p);
					}
				}
			}
endv4:
			if (host[0] != '\0') {
                     	if (inet_ntop(AF_INET, &ip->ip_src, src, sizeof(src)) == NULL)
				(void)strlcpy(src, "?", sizeof(src));
			printf("%s accessed %s\n", src, host);
			}

                     n = sendto(fd, packet, n, 0, (struct sockaddr *) &sin,
                         sin_len);
                     if (n == -1)
                             warn("sendto");
		}
		if (nfds > 0 && pfd[PFD_DIVERT6_SOCKET].revents & POLLIN) {
			memset(packet, 0, sizeof(packet));
                     n = recvfrom(fd6, packet, sizeof(packet), 0, (struct sockaddr *) &sin6, &sin6_len);
                     if (n == -1) {
                             warn("recvfrom");
                             continue;
                     }
                     if (n < (sizeof(struct ip6_hdr) + sizeof(struct tcphdr))) {
                             warnx("packet is too short");
                             continue;
                     }
			ip6_hdr = (struct ip6_hdr *) packet;
			memset(host, 0, sizeof(host));
			if (ip6_hdr->ip6_nxt == 0x06) {
                     th = (struct tcphdr *) (packet + 40);
			ptr = (void*)(th) + (th->th_off<<2);
/* HTTP packet = check Host header */
			f = strstr(ptr, "\r\n\r\n");
			if (f != NULL) {
				p = strstr(ptr, "Host: ");
				if (p != NULL) {
					f = strstr(p, "\r\n");
					if (f != NULL) {
						snprintf(host, f-p-5, "%s", p+6);
					}
				}
			} else {
/* HTTPS packet = check SNI option */
				if (ptr[0] == 0x16 && ptr[1] == 0x03 && ptr[5] == 0x01) {
					u_int8_t len8;
					u_int16_t len16;
					u_int32_t len32;
				/* SSLv3/TLS handshake & client hello.
				 * Search for Extension 0 : server_name
				 */
					p = ptr + 5; /* Start of ClientHello */
					len32 = (p[1] & 0x000000ff); len32 <<= 8;
					len32 += (p[2] & 0x000000ff); len32 <<= 8;
					len32 += (p[3] & 0x000000ff);
					if (len32 == 0) goto endv6;
					len8 = ptr[43];
					p = ptr + 43 + 1 + len8; /* SessionId */
					len16 = (p[0] & 0x00ff); len16 <<=8;
					len16 += (p[1] & 0x00ff);
					p = p + 2 + len16; /* CipherSuite */
					len8 = p[0];
					if (len8 == 0) goto endv6;
					p = p + 1 + len8; /* Compression */
					/* Extensions */
					len16 = (p[0] & 0x00ff); len16 <<=8;
					len16 += (p[1] & 0x00ff);
					extlen = len16;
					if (extlen == 0) goto endv6;
					p += 2;
					extlen -= 2;
					len16 = (p[0] & 0x00ff); len16 <<=8;
					len16 += (p[1] & 0x00ff);
					while (len16 != 0 && extlen > 0) {
						p += 2;
						len16 = (p[0] & 0x00ff); len16 <<=8;
						len16 += (p[1] & 0x00ff);
						extlen -= 2;
					}
					if (extlen == 0) goto endv6;
					else {
						p += 2;
						len16 = (p[0] & 0x00ff); len16 <<=8;
						len16 += (p[1] & 0x00ff);
						if (len16 == 0) goto endv6; // sni length
						p += 2;
						len16 = (p[0] & 0x00ff); len16 <<=8;
						len16 += (p[1] & 0x00ff);
						if (len16 == 0) goto endv6; // sni list length
						p += 4;
						len8 = p[0];
						p++;
						snprintf(host, len8+1, "%s", p);
					}
				}
			}
			}
endv6:
			if (host[0] != '\0') {
                     	if (inet_ntop(AF_INET6, &ip6_hdr->ip6_src, src, sizeof(src)) == NULL)
				strlcpy(src, "?", sizeof(src));
			printf("%s accessed %s\n", src, host);
			}
			sendto(fd6, packet, n, 0, (struct sockaddr *) &sin6, sin6_len);

		}
	}

             return 0;
     }

