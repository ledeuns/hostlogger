Quick and dirty logger of host

```
# printf "pass out on em0 inet6 proto tcp divert-packet port 700\n" | pfctl -f-
# printf "pass out on em0 inet proto tcp divert-packet port 700\n" | pfctl -f-

# doas ./hostlogger
2001:db8:1::10 accessed www.google.com
2001:db8:1::10 accessed ssl.gstatic.com
192.168.10.10 accessed twitter.com
```

PoC working but still needs some love (process separation, control program,
etc...)